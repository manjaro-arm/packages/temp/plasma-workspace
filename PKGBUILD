# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Maintainer: Antonio Rojas <arojas@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Alexey D. <lq07829icatm at rambler.ru>

pkgbase=plasma-workspace
pkgname=(plasma-workspace plasma-wayland-session)
pkgver=5.26.3
pkgrel=0.2
pkgdesc='KDE Plasma Workspace'
arch=(x86_64 aarch64)
url='https://kde.org/plasma-desktop/'
license=(LGPL)
depends=(knotifyconfig ksystemstats ktexteditor libqalculate kde-cli-tools appstream-qt
         xorg-xrdb xorg-xsetroot kactivitymanagerd kholidays xorg-xmessage milou prison kwin
         plasma-integration kpeople kactivities-stats libkscreen kquickcharts kuserfeedback kpipewire
         accountsservice kio-extras kio-fuse qt5-tools oxygen-sounds)
makedepends=(extra-cmake-modules kdoctools gpsd baloo networkmanager-qt plasma-wayland-protocols kunitconversion)
groups=(plasma)
source=(https://download.kde.org/stable/plasma/$pkgver/$pkgbase-$pkgver.tar.xz{,.sig} kde.pam
        https://invent.kde.org/plasma/plasma-workspace/-/commit/8056e09e08b37e19b1f55179fc8bfa960eea8e24.patch
        https://invent.kde.org/plasma/plasma-workspace/-/commit/a77f1bda64114712603a8c25bc2c678dc6957676.patch)
sha256sums=('0a2c1e98e3e540822db833a6f8d0a35a174d327e56a81756b3baf0d8ba35c953'
            'SKIP'
            '00090291204baabe9d6857d3b1419832376dd2e279087d718b64792691e86739'
            'be52d3f5790a07c9599fb5f54fa29c67d086b1f865d7700bed7e89ad3dc38b95'
            '4a8bc278824518a4eb898fd38d34c96900f6a8c3518ae29696336069532ab56d')
validpgpkeys=('E0A3EB202F8E57528E13E72FD7574483BB57B18D'  # Jonathan Esk-Riddell <jr@jriddell.org>
              '0AAC775BB6437A8D9AF7A3ACFE0784117FBCE11D'  # Bhushan Shah <bshah@kde.org>
              'D07BD8662C56CB291B316EB2F5675605C74E02CF'  # David Edmundson <davidedmundson@kde.org>
              '1FA881591C26B276D7A5518EEAAF29B42A678C20') # Marco Martin <notmart@gmail.com>

prepare() {
  cd $pkgbase-$pkgver
  #Shell: Always call load after init (added in 5.26.4)
  patch -Np1 -i "${srcdir}/8056e09e08b37e19b1f55179fc8bfa960eea8e24.patch"
  #partially revert previous patch (added in 5.26.4)
  patch -Np1 -i "${srcdir}/a77f1bda64114712603a8c25bc2c678dc6957676.patch"
}

build() {
  cmake -B build -S $pkgbase-$pkgver \
    -DCMAKE_INSTALL_LIBEXECDIR=lib \
    -DGLIBC_LOCALE_GEN=OFF \
    -DBUILD_TESTING=OFF
  cmake --build build
}

package_plasma-workspace() {
  optdepends=('plasma-workspace-wallpapers: additional wallpapers'
              'gpsd: GPS based geolocation' 'networkmanager-qt: IP based geolocation'
              'kdepim-addons: displaying PIM events in the calendar'
              'appmenu-gtk-module: global menu support for GTK2 and some GTK3 applications'
              'baloo: Baloo search runner' 'discover: manage applications installation from the launcher')
  backup=('etc/pam.d/kde')

  DESTDIR="$pkgdir" cmake --install build

  install -Dm644 "$srcdir"/kde.pam "$pkgdir"/etc/pam.d/kde

  # Split plasma-wayland scripts
  rm -r "$pkgdir"/usr/share/wayland-sessions
}

package_plasma-wayland-session() {
  pkgdesc='Plasma Wayland session'
  depends=(plasma-workspace qt5-wayland kwayland-integration xorg-xwayland)
  groups=()

  install -Dm644 build/login-sessions/plasmawayland.desktop "$pkgdir"/usr/share/wayland-sessions/plasmawayland.desktop
}
